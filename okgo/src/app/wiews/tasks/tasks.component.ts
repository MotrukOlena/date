import { Component, OnInit } from '@angular/core';
import {Task} from "../../model/task";
import {DataHenglerService} from "../../servies/data-hengler.service";
import {subscribeOn} from "rxjs/operators";

@Component({
  selector: 'app-task',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  tasks: Task[];

  constructor(private dataHengler: DataHenglerService) { }

  ngOnInit(): void {
   this.dataHengler.taskSubject.subscribe(tasks => this.tasks = tasks)
  }

  toggleTaskComplide(task: Task) {
    task.completed = ! task.completed
  }
}
