import {Component, OnInit} from '@angular/core';
import {DataHenglerService} from "../../servies/data-hengler.service";
import {Category} from "../../model/Category";

@Component({
  selector: 'app-cotegories',
  templateUrl: './cotegories.component.html',
  styleUrls: ['./cotegories.component.scss']
})
export class CotegoriesComponent implements OnInit {

  selectedCategory: Category;

  categories: Category[];

  constructor(private dataHengler: DataHenglerService) {
  }

  ngOnInit(): void {
    this.dataHengler.categoresSubject.subscribe(categories => this.categories = categories)
  }

  showTasksMeCotegory(category: Category) {
    this.selectedCategory = category;
    this.dataHengler.fillTasksCoategory(category)
  }
}
