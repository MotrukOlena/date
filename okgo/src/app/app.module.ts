import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CotegoriesComponent} from './wiews/cotegories/cotegories.component';
import {TasksComponent} from './wiews/tasks/tasks.component';

@NgModule({
  declarations: [
    AppComponent,
    CotegoriesComponent,
    TasksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
