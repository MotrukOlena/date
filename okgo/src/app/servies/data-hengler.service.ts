import {Injectable} from '@angular/core';
import {Category} from "../model/Category";
import {TestData} from "../date/TestData";
import {Task} from "../model/task";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataHenglerService {

  taskSubject = new BehaviorSubject<Task[]>(TestData.tasks);
  categoresSubject = new BehaviorSubject<Category[]>(TestData.categories)

  constructor() {
  }

  // getCategories(): Category[] {
  //   return TestData.categories;
  // }

  fillTasks() {
    this.taskSubject.next(TestData.tasks);
  }

  fillTasksCoategory(Category: Category) {
    const tasks = TestData.tasks.filter(task => task.category === Category)
    this.taskSubject.next(tasks);
  }
}
