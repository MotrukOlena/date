import {TestBed} from '@angular/core/testing';

import {DataHenglerService} from './data-hengler.service';

describe('DataHenglerService', () => {
  let service: DataHenglerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataHenglerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
